# Usage
In current version it is a js console emulator with code highlighting, which works also on mobile
devices. All *console.log* instructions a mocked up, but environment. Then to execute it is necessary
to click or tap on icon, which is on the left of the text field. The collected output of
console.log will be printed below.

# Pages

[Try it!](https://asanokhin.gitlab.io/dashboard.js/)